import { db } from "@/plugins/firebase";

export const state = () => ({
  tareas: [],
  tarea: {}
})

export const mutations = {
  setTareas (state, tareas) {
    state.tareas = tareas
  },
  setTarea (state, tarea) {
    state.tareas.push(tarea)
  },
  deleteTarea (state, tarea) {
    const index = state.tareas.findIndex(item => item.id === tarea.id)
    state.tareas.splice(index, 1)
  },
  updateTarea (state, tarea) {
    const index = state.tareas.findIndex(item => item.id === tarea.id)
    state.tareas[index].nombre = tarea.nombre
  },
  setTarea (state , tarea) {
    state.tarea = tarea
  }
}

export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    return db.collection('tareas').get()
      .then(data => {
        const tareas = []
        data.forEach(element => {
          let tarea = element.data()
          tarea.id = element.id
          tareas.push(tarea)
        });
        return commit('setTareas', tareas)
      })
      .catch(function (error) {
        console.log("Error: ", error)
      })
  },
  async agregarTarea ({ commit }, tarea) {
    try {
      const res = await db.collection('tareas').add({
        nombre: tarea,
        fecha: new Date()
      })
      commit('setTarea', {
        id: res.id,
        nombre: tarea
      })
      this.app.router.push('/vuex')
    } catch (e) {
      console.log(e);
    }
  },
  eliminarTarea ({ commit }, tarea) {
    db.collection('tareas').doc(tarea.id).delete()
      .then(function() {
        console.log("Borrado satisfactorio");
        commit('deleteTarea', tarea)
      })
      .catch(function(e) {
        console.log("Error: ", e);
      })
  },
  editarTarea ({ commit }, tarea) {
    db.collection('tareas').doc(tarea.id).update({
      nombre: tarea.nombre
    })
    .then(() => {
      commit('updateTarea', tarea)
      this.app.router.push('/vuex')
    })
    .catch( e => {
      console.log(e);
    })
  }
}
